import { useState } from 'react';

export const useForceUpdate = () => {
  const [value, setValue] = useState(0); // integer state
  return [value, () => setValue((value) => ++value)] as const; // update the state to force render
};
