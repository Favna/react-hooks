import { Dispatch, SetStateAction, useEffect, useRef, useState } from 'react';

type Callback<S> = (state: S) => void | (() => void | undefined);
type DispatchWithCallback<S, A> = (value: A, callback: Callback<S>) => void;

export const useStateWithCallback = <S>(initialState: S, callback: Callback<S>): [S, Dispatch<SetStateAction<S>>] => {
  const [state, setState] = useState(initialState);

  useEffect(() => callback(state), [state, callback]);

  return [state, setState];
};

export const useStateWithCallbackLazy = <S>(initialValue: S): [S, DispatchWithCallback<S, SetStateAction<S>>] => {
  const callbackRef = useRef(null);

  const [value, setValue] = useState(initialValue);

  useEffect(() => {
    if (callbackRef.current) {
      callbackRef.current(value);

      callbackRef.current = null;
    }
  }, [value]);

  const setValueWithCallback = (newValue, callback) => {
    callbackRef.current = callback;

    return setValue(newValue);
  };

  return [value, setValueWithCallback];
};
