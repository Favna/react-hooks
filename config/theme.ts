import { blueGrey, grey, red, yellow } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: red,
    secondary: blueGrey,
    error: yellow,
    background: {
      default: grey[900]
    }
  }
});

export default theme;
