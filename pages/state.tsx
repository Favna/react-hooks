import { NextPage } from 'next';
import React from 'react';
import Layout from '../components/Layout';
import StateManagement from '../components/StateManagement';
import { User } from '../models/User';

const State: NextPage = () => {
  const user: User = {
    name: 'Jeroen Claassens',
    email: 'j.claassens@cgi.com'
  };

  return <Layout items={[<StateManagement user={user} />]} />;
};

export default State;
