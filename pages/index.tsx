import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { NextPage } from 'next';
import React from 'react';
import Layout from '../components/Layout';
import Link from '../components/Link';

const Home: NextPage = () => {
  return (
    <Layout
      items={[
        <Link href="/state">
          <Button fullWidth variant="contained" color="primary">
            <Typography variant="h6" color="textPrimary" component="span">
              Go to the state page
            </Typography>
          </Button>
        </Link>,

        <Link href="/lifecycle">
          <Button fullWidth variant="contained" color="primary">
            <Typography variant="h6" color="textPrimary" component="span">
              Go to lifecycle page
            </Typography>
          </Button>
        </Link>,
        <Link href="/context">
          <Button fullWidth variant="contained" color="primary">
            <Typography variant="h6" color="textPrimary" component="span">
              Go to the context page
            </Typography>
          </Button>
        </Link>,
        <Link href="/callbacks">
          <Button fullWidth variant="contained" color="primary">
            <Typography variant="h6" color="textPrimary" component="span">
              Go to the callbacks page
            </Typography>
          </Button>
        </Link>,
        <Link href="/state-with-callback">
          <Button fullWidth variant="contained" color="primary">
            <Typography variant="h6" color="textPrimary" component="span">
              Go to state with callback page
            </Typography>
          </Button>
        </Link>,
        <Link href="/state-with-lazy-callback">
          <Button fullWidth variant="contained" color="primary">
            <Typography variant="h6" color="textPrimary" component="span">
              Go to state with lazy callback page
            </Typography>
          </Button>
        </Link>,
        <Link href="/state-with-reducer">
          <Button fullWidth variant="contained" color="primary">
            <Typography variant="h6" color="textPrimary" component="span">
              Go to the state with reducer page
            </Typography>
          </Button>
        </Link>,
        <Link href="/memoization">
          <Button fullWidth variant="contained" color="primary">
            <Typography variant="h6" color="textPrimary" component="span">
              Go to memoization page
            </Typography>
          </Button>
        </Link>
      ]}
    />
  );
};

export default Home;
