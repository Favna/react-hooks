import { NextPage } from 'next';
import React from 'react';
import Layout from '../components/Layout';
import MemoizationManagement from '../components/MemoizationManagement';

const Memoization: NextPage = () => {
  return <Layout items={[<MemoizationManagement />]} />;
};

export default Memoization;
