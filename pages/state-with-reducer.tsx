import { NextPage } from 'next';
import React from 'react';
import Layout from '../components/Layout';
import StateReducer from '../components/StateReducer';

const StateWithReducer: NextPage = () => {
  return <Layout items={[<StateReducer />]} />;
};

export default StateWithReducer;
