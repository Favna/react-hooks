import { NextPage } from 'next';
import React from 'react';
import ContextManagement from '../components/ContextManagement';
import Layout from '../components/Layout';

const Context: NextPage = () => {
  return <Layout items={[<ContextManagement />]} />;
};

export default Context;
