import { NextPage } from 'next';
import React from 'react';
import CallbackManagement from '../components/CallbackManagement';
import Layout from '../components/Layout';

const Callbacks: NextPage = () => {
  return <Layout items={[<CallbackManagement />]} />;
};

export default Callbacks;
