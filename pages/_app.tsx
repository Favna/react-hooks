import { useMediaQuery } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import { StylesProvider, ThemeProvider } from '@material-ui/core/styles';
import { NextPage } from 'next';
import { AppProps } from 'next/app';
import Head from 'next/head';
import React, { useEffect } from 'react';
import { MobileContextProvider } from '../config/MobileContext';
import theme from '../config/theme';

const App: NextPage<AppProps> = ({ Component, pageProps }) => {
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <>
      <Head>
        <title>Sample app</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
      </Head>

      <StylesProvider injectFirst>
        <ThemeProvider theme={theme}>
          <MobileContextProvider value={{ isMobile }}>
            <CssBaseline />
            <Component {...pageProps} />
          </MobileContextProvider>
        </ThemeProvider>
      </StylesProvider>
    </>
  );
};

export default App;
