import { Button } from '@material-ui/core';
import { NextPage } from 'next';
import React, { useState } from 'react';
import Layout from '../components/Layout';
import LifeCycleManagement from '../components/LifeCycleManagement';

const LifeCycle: NextPage = () => {
  const [iterator, setIterator] = useState(0);

  return (
    <Layout
      items={[
        <LifeCycleManagement iterator={iterator} />,
        <Button fullWidth onClick={() => setIterator(iterator + 1)} variant="contained" color="primary">
          Increase Iterator
        </Button>
      ]}
    />
  );
};

export default LifeCycle;
