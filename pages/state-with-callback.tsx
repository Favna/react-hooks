import { NextPage } from 'next';
import React from 'react';
import Layout from '../components/Layout';
import StateCallback from '../components/StateCallback';

const StateWithCallback: NextPage = () => {
  return <Layout items={[<StateCallback />]} />;
};

export default StateWithCallback;
