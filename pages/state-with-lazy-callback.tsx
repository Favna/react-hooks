import { NextPage } from 'next';
import React from 'react';
import Layout from '../components/Layout';
import StateLazyCallback from '../components/StateLazyCallback';

const StateWithLazyCallback: NextPage = () => {
  return <Layout items={[<StateLazyCallback />]} />;
};

export default StateWithLazyCallback;
