import { Button, Dialog, DialogContent, DialogTitle, TextField, Typography } from '@material-ui/core';
import React, { ChangeEvent, FC, useState } from 'react';
import { User } from '../models/User';

interface Props {
  user: User;
}

interface State {
  dialogOpen: boolean;
  fieldValue: string;
}

const defaultState: State = {
  dialogOpen: false,
  fieldValue: ''
}

const StateManagement: FC<Props> = ({ user }) => {
  const [dialogOpen, setDialogOpen] = useState(false);
  const [fieldValue, setFieldValue] = useState(user.email);
  const [state, setState] = useState<State>(defaultState);

  const handleToggleDialog = () => setDialogOpen(!dialogOpen);
  const handleToggleDialog2 = () => setState({...state, dialogOpen: !state.dialogOpen });

  const handleTextFieldChange = (event: ChangeEvent<HTMLInputElement>) => setFieldValue(event.target.value);

  return (
    <>
      <Button fullWidth onClick={handleToggleDialog} variant="contained" color="primary">
        Click me to open the dialog
      </Button>
      <Dialog open={dialogOpen} onBackdropClick={handleToggleDialog} onEscapeKeyDown={handleToggleDialog}>
        <DialogTitle>Enter email address</DialogTitle>
        <DialogContent>
          <TextField fullWidth defaultValue="" placeholder="Enter a new email address" onChange={handleTextFieldChange} />
          <Typography>The entered value is: {fieldValue}</Typography>
          <Button fullWidth onClick={handleToggleDialog} variant="contained" color="secondary" style={{ marginTop: '1rem' }}>
            Ok
          </Button>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default StateManagement;
