import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import React, { FC } from 'react';
import { useStateWithCallback } from '../helpers/useStateWithCallback';

const StateCallback: FC = () => {
  const [count, setCount] = useStateWithCallback(0, (count) => {
    if (count > 1) {
      console.log('render, then callback.');
    }
      console.log('otherwise use useStateWithCallbackInstant()');
  });

  const handleClick = () => {
    setCount(count + 1);
  };

  return (
    <Grid container spacing={10} direction="row" justify="center" alignItems="center" alignContent="center">
      <Grid item xs={12} style={{ textAlign: 'center' }}>
        <Typography color="primary" variant="h6" align="center">
          Counted: {count}
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Button fullWidth variant="contained" color="primary" onClick={handleClick}>
          Increase
        </Button>
      </Grid>
    </Grid>
  );
};

export default StateCallback;
