import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import React, { FC, useMemo } from 'react';
import { useForceUpdate } from '../helpers/useForceUpdate';

const MemoizationManagement: FC = () => {
  const [forcedUpdateValue, forceUpdate] = useForceUpdate();

  const BigDataMemo = useMemo(() => {
    console.log('computing array entries for BigDataMemo');
    return new Array(10).fill(undefined).map(() => {
      return Math.floor(Math.random() * 10);
    });
  }, []);

  const BigDataNonMemo = () => {
    console.log('computing array entries for BigDataNonMemo');
    return new Array(10).fill(undefined).map(() => {
      return Math.floor(Math.random() * 10);
    });
  };

  return (
    <Grid container spacing={10} direction="row" justify="center" alignItems="center" alignContent="center">
      <Grid item xs={12} style={{ textAlign: 'center' }}>
        <Typography color="primary" variant="h6" align="center">
          BigDataMemo entries: {BigDataMemo.join(', ')}
        </Typography>
      </Grid>
      <Grid item xs={12} style={{ textAlign: 'center' }}>
        <Typography color="primary" variant="h6" align="center">
          BigDataNonMemo entries: {BigDataNonMemo().join(', ')}
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Button variant="contained" color="secondary" fullWidth onClick={forceUpdate}>
          <Typography variant="body2">Click to re-render - re-rendered a total of {forcedUpdateValue} times</Typography>
        </Button>
      </Grid>
    </Grid>
  );
};

export default MemoizationManagement;
