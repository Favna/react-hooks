import { Typography } from '@material-ui/core';
import React, { FC, useEffect } from 'react';

interface LifeCycleManagementProps {
  iterator: number;
}

const parseOrdinal = (cardinal: number) => {
  const cent = cardinal % 100;
  const dec = cardinal % 10;

  if (cent >= 10 && cent <= 20) {
    return `${cardinal}th`;
  }

  switch (dec) {
    case 1:
      return `${cardinal}st`;
    case 2:
      return `${cardinal}nd`;
    case 3:
      return `${cardinal}rd`;
    default:
      return `${cardinal}th`;
  }
};

const LifeCycleManagement: FC<LifeCycleManagementProps> = ({ iterator }) => {
  useEffect(() => {
    console.log(`re-rendering component for the ${parseOrdinal(iterator)} time`);

    return () => console.log('cleaning up useEffect run for: ', iterator);
  }, [iterator]);

  return (
    <>
      <Typography color="primary" variant="h6" align="center">
        Counted: {iterator}
      </Typography>
    </>
  );
};

export default LifeCycleManagement;
