import { Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { useMobileContext } from '../config/MobileContext';

const ContextManagement: FC = () => {
  const { isMobile } = useMobileContext();

  return (
    <>
      <Typography color="primary" variant="h6">
        Mobile view will be enabled: {isMobile ? 'Yes' : 'No'}
      </Typography>
    </>
  );
};

export default ContextManagement;
