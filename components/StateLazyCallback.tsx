import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import React, { FC } from 'react';
import { useStateWithCallbackLazy } from '../helpers/useStateWithCallback';

const StateLazyCallback: FC = () => {
  const [count, setCount] = useStateWithCallbackLazy(0);

  const handleClick = () => {
    setCount(count + 1, (currentCount) => {
      if (currentCount > 1) {
        console.log('Threshold of over 1 reached.');
      } else {
        console.log('No threshold reached.');
      }
    });
  };

  return (
    <Grid container spacing={10} direction="row" justify="center" alignItems="center" alignContent="center">
      <Grid item xs={12} style={{ textAlign: 'center' }}>
        <Typography color="primary" variant="h6" align="center">
          Counted: {count}
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Button fullWidth variant="contained" color="primary" onClick={handleClick}>
          Increase
        </Button>
      </Grid>
    </Grid>
  );
};

export default StateLazyCallback;
