import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { mergeDefault } from '@sapphire/utilities';
import React, { FC, useReducer } from 'react';

type MakeReducerActions<A> = {
  type: A;
};

interface State {
  counter: number;
}

enum StateReducerActions {
  Increment,
  Decrement
}

const reducer = (state: State, action: MakeReducerActions<StateReducerActions>): State => {
  switch (action.type) {
    case StateReducerActions.Increment: {
      return mergeDefault(state, { counter: state.counter + 1 });
    }
    case StateReducerActions.Decrement: {
      return mergeDefault(state, { counter: state.counter - 1 });
    }
    default: {
      throw new Error();
    }
  }
};

const StateReducer: FC = () => {
  const [state, dispatch] = useReducer(reducer, { counter: 0 });

  return (
    <Grid container spacing={10} direction="row" justify="center" alignItems="center" alignContent="center">
      <Grid item xs={12} style={{ textAlign: 'center' }}>
        <Typography color="primary" variant="h6" align="center">
          Counted: {state.counter}
        </Typography>
      </Grid>
      <Grid item xs={6}>
        <Button fullWidth variant="contained" color="primary" onClick={() => dispatch({ type: StateReducerActions.Increment })}>
          +
        </Button>
      </Grid>
      <Grid item xs={6}>
        <Button fullWidth variant="contained" color="primary" onClick={() => dispatch({ type: StateReducerActions.Decrement })}>
          -
        </Button>
      </Grid>
    </Grid>
  );
};

export default StateReducer;
