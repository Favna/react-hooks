import Container from '@material-ui/core/Container';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import HomeIcon from '@material-ui/icons/Home';
import { useRouter } from 'next/router';
import React, { FC, ReactNode } from 'react';

interface LayoutProps {
  items: ReactNode[];
}

const useStyles = makeStyles(() =>
  createStyles({
    pageHeight: {
      maxHeight: '100vh',
      minHeight: '100vh'
    }
  })
);

const Layout: FC<LayoutProps> = ({ items }) => {
  const classes = useStyles();
  const router = useRouter();

  return (
    <Container maxWidth="lg" classes={{ root: classes.pageHeight }}>
      <Grid container spacing={10} direction="row" justify="center" alignItems="center" alignContent="center" classes={{ root: classes.pageHeight }}>
        {items.map((item, index) => (
          <Grid key={index} item xs={6}>
            {item}
          </Grid>
        ))}
      </Grid>
      <Fab color="primary" aria-label="Home" onClick={() => router.push('/')} style={{ position: 'absolute', right: '80px' }}>
        <HomeIcon />
      </Fab>
    </Container>
  );
};

export default Layout;
