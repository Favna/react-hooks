import { Typography } from '@material-ui/core';
import React, { FC, useCallback, useState } from 'react';
import { useMobileContext } from '../config/MobileContext';

const CallbackManagement: FC = () => {
  const [height, setHeight] = useState(0);
  const { isMobile } = useMobileContext();

  const measuredRef = useCallback((node) => {
    if (node !== null) {
      setHeight(node.getBoundingClientRect().height);
    }
  }, []);

  return (
    <>
      <Typography ref={measuredRef} color="primary" variant={isMobile ? 'h4' : 'h1'}>
        A very nice header
      </Typography>
      <Typography color="secondary" variant="h6">
        The input input has a height of: {Math.round(height)} px
      </Typography>
    </>
  );
};

export default CallbackManagement;
